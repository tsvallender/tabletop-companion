# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_05_09_200914) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "characters", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "name", null: false
    t.string "sheet_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "table_id"
    t.index ["table_id"], name: "index_characters_on_table_id"
    t.index ["user_id"], name: "index_characters_on_user_id"
  end

  create_table "combat_trackers", force: :cascade do |t|
    t.string "name"
    t.bigint "table_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["table_id"], name: "index_combat_trackers_on_table_id"
  end

  create_table "combatants", force: :cascade do |t|
    t.bigint "character_id", null: false
    t.bigint "zone_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["character_id"], name: "index_combatants_on_character_id"
    t.index ["zone_id"], name: "index_combatants_on_zone_id"
  end

  create_table "tables", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.string "slug", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "active_combat_id"
    t.index ["active_combat_id"], name: "index_tables_on_active_combat_id"
    t.index ["name"], name: "index_tables_on_name", unique: true
    t.index ["slug"], name: "index_tables_on_slug", unique: true
    t.index ["user_id"], name: "index_tables_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "zones", force: :cascade do |t|
    t.bigint "combat_tracker_id", null: false
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["combat_tracker_id"], name: "index_zones_on_combat_tracker_id"
  end

  add_foreign_key "characters", "tables"
  add_foreign_key "characters", "users"
  add_foreign_key "combat_trackers", "tables"
  add_foreign_key "combatants", "characters"
  add_foreign_key "combatants", "zones"
  add_foreign_key "tables", "combat_trackers", column: "active_combat_id"
  add_foreign_key "tables", "users"
  add_foreign_key "zones", "combat_trackers"
end
