class CreateZones < ActiveRecord::Migration[7.0]
  def change
    create_table :zones do |t|
      t.references :combat_tracker, null: false, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
