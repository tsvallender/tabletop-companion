class CreateCombatTrackers < ActiveRecord::Migration[7.0]
  def change
    create_table :combat_trackers do |t|
      t.string :name
      t.references :table, null: false, foreign_key: true

      t.timestamps
    end
  end
end
