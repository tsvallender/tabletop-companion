class AddActiveCombatToTable < ActiveRecord::Migration[7.0]
  def change
    add_reference :tables, :active_combat, null: true, foreign_key: { to_table: :combat_trackers }
  end
end
