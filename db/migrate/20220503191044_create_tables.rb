class CreateTables < ActiveRecord::Migration[7.0]
  def change
    create_table :tables do |t|
      t.string :name, null: false
      t.string :description
      t.string :slug, null: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :tables, :name, unique: true
    add_index :tables, :slug, unique: true
  end
end
