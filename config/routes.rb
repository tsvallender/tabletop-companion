Rails.application.routes.draw do
  root 'tables#index'
  devise_for :users

  resources :characters, only: [:index, :new, :create, :edit, :update]
  resources :tables, only: [:index, :show, :new, :create, :update] do
    resources :combat_trackers, only: [:new, :create]
  end
end
