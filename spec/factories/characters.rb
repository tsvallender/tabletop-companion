FactoryBot.define do
  factory :character do
    user
    name { Faker::Fantasy::Tolkien.character }

    trait :with_sheet_url do
      sheet_url { Faker::Internet.url }
    end

    trait :with_table do
      association :table
    end
  end
end
