FactoryBot.define do
  factory :user do
    email         { Faker::Internet.unique.email }
    password      { Faker::Internet.password }
    confirmed_at  { Time.now }

    trait :with_characters do
      after(:build) do |user|
        user.characters = build_list(:character, Random.rand(1..6), :with_sheet_url, :with_table)
      end
    end

    trait :with_tables do
      after(:build) do |user|
        user.tables = build_list(:table, Random.rand(1..3))
      end
    end
  end
end
