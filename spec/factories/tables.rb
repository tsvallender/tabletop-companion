FactoryBot.define do
  factory :table do
    name { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    slug { Faker::Internet.slug }
    user
  end
end
