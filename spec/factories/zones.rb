FactoryBot.define do
  factory :zone do
    name { Faker::Lorem.sentence }
    combat_tracker
  end
end
