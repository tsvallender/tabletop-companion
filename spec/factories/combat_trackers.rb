FactoryBot.define do
  factory :combat_tracker do
    name { Faker::Fantasy::Tolkien.location }
    table

    trait :with_zones do
      after(:build) do |combat_tracker|
        combat_tracker.zones = build_list(:zone, Random.rand(3..8))
      end
    end
  end
end
