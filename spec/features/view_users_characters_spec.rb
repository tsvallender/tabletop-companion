require 'rails_helper'

feature 'user views characters' do
  let!(:user) { create :user, :with_characters }

  scenario 'successfully' do
    sign_in user
    visit root_path
    expect(page).to have_css 'a', text: 'My characters'
    click_link 'My characters'
    user.characters.each do |character|
      expect(page).to have_css 'h3', text: character.name
      expect(page).to have_css 'a', text: "Edit #{character.name}"
    end
  end
end
