require 'rails_helper'

feature 'user views tables' do
  let!(:user) { create :user, :with_characters, :with_tables }

  scenario 'successfully' do
    sign_in user
    visit root_path
    expect(page).to have_css 'h2', text: 'My tables'
    user.characters.each do |character|
      if character.table
        expect(page).to have_css 'a', id: "table-#{character.table.id}",
                                        text: "#{character.table.name}"
      end
    end
  end
end
