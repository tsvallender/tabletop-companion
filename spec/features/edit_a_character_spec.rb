require 'rails_helper'

feature 'edits a character' do
  let!(:user) { create :user, :with_characters }

  scenario 'successfully' do
    sign_in user
    char = user.characters.first
    visit characters_path
    expect(page).to have_css 'a', id: "edit-#{char.id}"
    click_link "edit-#{char.id}"
    fill_in 'character_name', with: 'Raistlin Majere'
    fill_in 'character_sheet_url', with: 'https://dndbeyond.com/my_sheet'
    click_button "Update character"
    char.reload
    expect(char.name).to eq 'Raistlin Majere'
    expect(char.sheet_url).to eq 'https://dndbeyond.com/my_sheet'
    expect(page).to have_text 'Raistlin Majere updated!'
  end
end
