require 'rails_helper'

feature 'user views characters' do
  let!(:user) { create :user, :with_characters, :with_tables }

  scenario 'successfully' do
    sign_in user
    visit table_path(user.tables.first)
    expect(page).to have_css 'a', text: 'New combat'
    click_link 'New combat'

    fill_in 'combat_tracker_name', with: 'My combat'

    expect { click_button "Create combat" }
      .to change { user.tables.first.combat_trackers.count }.by(1)
  end
end
