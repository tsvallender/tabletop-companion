require 'rails_helper'

feature 'create a character' do
  let!(:user) { create :user }

  scenario 'successfully' do
    sign_in user
    visit characters_path
    click_link "Add a character"
    fill_in 'character_name', with: 'Raistlin Majere'
    fill_in 'character_sheet_url', with: 'https://dndbeyond.com/my_sheet'
    expect { click_button "Add character" }.to change { user.characters.count }.by(1)
    expect(page).to have_text 'Raistlin Majere created!'
  end
end
