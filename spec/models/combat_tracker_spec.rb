require 'rails_helper'

RSpec.describe CombatTracker, type: :model do
  let!(:users) { build_list(:user, 3, :with_characters, :with_tables) }

  context "when starting a new combat" do
    it "allows zones to be added" do
      user = users.first
      tracker = user.tables.first.combat_trackers.build(name: 'Exciting combat')
      tracker.zones.build(name: 'Zone 1')
      tracker.zones.build(name: 'Zone 2')
      tracker.zones.build(name: 'Zone 3')
      tracker.save!
      expect(tracker.zones.count).to eq(3)
    end
  end

  context "when not in a combat" do
    let!(:tracker) { create :combat_tracker, :with_zones }

    it "allows a player to add their character" do
      tracker.move(
        character: users.first.characters.first,
        zone: tracker.zones.first)
      tracker.move(
        character: users.second.characters.first,
        zone: tracker.zones.first)

      expect(tracker.zones.first.characters.count).to eq 2
    end
  end

  context "when in a combat" do
    let!(:tracker) { create :combat_tracker, :with_zones }

    it "allows a player to move their character" do
      tracker.move(
        character: users.first.characters.first,
        zone: tracker.zones.first)
      tracker.move(
        character: users.first.characters.first,
        zone: tracker.zones.first)
      tracker.move(
        character: users.first.characters.first,
        zone: tracker.zones.second)

        expect(tracker.zones.second.characters.count).to eq 1
        assert tracker.zones.first.characters.empty?
    end
  end
end
