class TablesController < ApplicationController
  before_action :set_table, only: [:show, :update]

  def index
    @tables = current_user.tables
    @characters = current_user.characters
  end

  def new
    @table = Table.new
  end

  def create
    table = current_user.tables.build(table_params)
    table.slug = table.name.parameterize

    if table.save!
      flash[:notice] = "#{table.name} created!"
      redirect_to tables_path
    else
      flash[:alert] = 'Unable to create table'
      render 'new'
    end
  end

  def show
    @character = Character.find_by(table: @table)
    render layout: false
  end

  def update
    if @table.update(table_params)
      flash[:notice] = "#{@table.name} updated!"
      redirect_to @table
    end
  end

  private

  def table_params
    params.require(:table).permit(:name, :description, :active_combat_id)
  end

  def set_table
    @table = Table.find(params[:id])
  end
end
