class CharactersController < ApplicationController
  before_action :set_character, only: [:edit, :update]

  def index
    @characters = current_user.characters
  end

  def new
    @character = Character.new
  end

  def create
    character = current_user.characters.build(character_params)

    if character.save!
      flash[:notice] = "#{character.name} created!"
      redirect_to characters_path
    else
      flash[:alert] = 'Unable to create character'
      render 'new'
    end
  end

  def edit
  end

  def update
    if @character.update(character_params)
      flash[:notice] = "#{@character.name} updated!"
      redirect_to characters_path
    end
  end

  private

  def character_params
    params.require(:character).permit(:name, :sheet_url, :table_id)
  end

  def set_character
    @character = Character.find(params[:id])
  end
end
