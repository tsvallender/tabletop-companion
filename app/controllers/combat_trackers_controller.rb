class CombatTrackersController < ApplicationController
  def new
    @combat_tracker = CombatTracker.new
  end

  def create
    table = Table.find(params[:table_id])
    combat_tracker = table.combat_trackers.build(combat_tracker_params)

    if combat_tracker.save!
      flash[:notice] = "#{combat_tracker.name} created!"
      redirect_to table
    else
      flash[:alert] = 'Unable to create combat tracker'
      render 'new'
    end
  end

  private

  def combat_tracker_params
    params.require(:combat_tracker).permit(:name)
  end
end
