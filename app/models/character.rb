class Character < ApplicationRecord
  belongs_to :user
  belongs_to :table, optional: true
  has_many :zones, through: :combatants
end
