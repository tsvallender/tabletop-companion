class CombatTracker < ApplicationRecord
  belongs_to :table
  has_many :zones

  def move(character:, zone:)
    zone.move(character: character)
  end
end
