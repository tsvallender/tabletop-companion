class Table < ApplicationRecord
  belongs_to :user
  has_many :combat_trackers
  belongs_to :active_combat, optional: true, class_name: "CombatTracker"
end
