class Zone < ApplicationRecord
  belongs_to :combat_tracker
  has_many :combatants
  has_many :characters, through: :combatants

  def move(character:)
    combatant = Combatant.find_or_initialize_by(character: character)
    combatant.zone = self
    combatant.save!
  end
end
