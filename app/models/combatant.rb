class Combatant < ApplicationRecord
  belongs_to :character
  belongs_to :zone
end
