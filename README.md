# Tabletop Companion

A companion to your online roleplaying sessions, to be used alongside
the video or voice calling platform of your choice, or for text-only
games.
